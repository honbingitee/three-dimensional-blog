/** @type {import('next').NextConfig} */
const nextConfig = {
  // reactStrictMode: true,
  swcMinify: true,
  // 不处理glb  next居然无法处理glb文件
  webpack: (config, options) => {
    config.module.rules.push({
      test: /\.glb/,
      use: [
        options.defaultLoaders.babel,
        {
          loader: 'file-loader'
        },
      ],
    })

    return config
  },
}

module.exports = nextConfig
