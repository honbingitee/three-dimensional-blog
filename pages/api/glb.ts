import type { NextApiRequest, NextApiResponse } from "next";
import path from "path";
import fs from "fs";

/**
 * 返回模型文件 .glb
 */
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { name } = req.query;
  if (!name) res.status(500).send("未提供name字段");
  const filePath = path.resolve("./public/model", name as string);
  const stat = fs.statSync(filePath);

  res.writeHead(200, {
    "Content-Type": "application/octet-stream",
    "Content-Length": stat.size,
  });

  const readStream = fs.createReadStream(filePath);
  readStream.pipe(res);
}
