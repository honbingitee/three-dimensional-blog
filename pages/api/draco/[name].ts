/*
 * @Author: hongbin
 * @Date: 2022-09-05 20:36:00
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-05 20:48:29
 * @Description: 返回 draco 文件 供解析glb模型使用
 */
import type { NextApiRequest, NextApiResponse } from "next";
import path from "path";
import fs from "fs";

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const {
    query: { name },
  } = req;

  const filePath = path.resolve("./public/draco", name as string);
  const stat = fs.statSync(filePath);

  res.writeHead(200, {
    "Content-Type": "application/octet-stream",
    "Content-Length": stat.size,
  });

  const readStream = fs.createReadStream(filePath);
  readStream.pipe(res);
}
