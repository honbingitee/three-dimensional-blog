// import "../src/styles/app.css";
import type { AppProps } from "next/app";
import { useEffect } from "react";
import MessageBar from "../src/HUI/MessageBar";
import GlobalStyle from "../src/styled/GlobalStyle";
import ThemeProvider from "../src/styled/ThemeProvide";

function MyApp({ Component, pageProps }: AppProps) {
    useEffect(() => {
        //! 这里代码居然在index.tsx的useEffect后执行 要注意初始化变量不能在此处
        // const initGlobalVariable = () => {
        //     window.alreadyInit = false;
        //     console.log("组件 加载");
        // };
        //initGlobalVariable();
    }, []);

    return (
        <ThemeProvider>
            <MessageBar />
            <GlobalStyle />
            <Component {...pageProps} />
        </ThemeProvider>
    );
}

export default MyApp;
