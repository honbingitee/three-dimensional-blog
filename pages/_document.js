/*
 * @Author: hongbin
 * @Date: 2022-09-04 15:23:15
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-10 21:52:22
 * @Description:  自定义 document结构 不能使用window对象
 */
import Document, { Html, Head, Main, NextScript } from 'next/document'
import { ServerStyleSheet } from 'styled-components'

const resetStyles = `
    html,body {
        height: 100%;
        font-family: "ruyi","Arial Rounded MT Bold", "Helvetica Rounded", Arial, sans-serif;
        background: #16ae10;
    }
    @font-face {
        font-family: "ruyi";
        src: url(/font/metaVo.ttf) format("TrueType");
    }

  ::-webkit-scrollbar-thumb {
    background: var(--deep-color);
    /* border-radius: 4px; */
}
::-webkit-scrollbar {
    width: 4px;
    height: 4px;
    background-color: rgb(38 174 14);
}
`;

class MyDocument extends Document {
    // styled-components 文档的支持ssr的书写方式 (样式有问题 不推荐)
    // static async getInitialProps(ctx) {
    //     const { renderPage } = ctx;
    //     const sheet = new ServerStyleSheet();
    //     const page = renderPage((Component) => (props) => sheet.collectStyles(<Component {...props} />));
    //     const styleElements = sheet.getStyleElement();
    //     const initialProps = await Document.getInitialProps(ctx)
    //     return { ...initialProps, styles: [initialProps.styles, styleElements] };
    // }

    static async getInitialProps(ctx) {
        const sheet = new ServerStyleSheet()
        const originalRenderPage = ctx.renderPage

        try {
            ctx.renderPage = () =>
                originalRenderPage({
                    enhanceApp: (App) => (props) =>
                        sheet.collectStyles(<App {...props} />),
                })

            const initialProps = await Document.getInitialProps(ctx)
            return {
                ...initialProps,
                styles: [initialProps.styles, sheet.getStyleElement()],
            }
        } finally {
            sheet.seal()
        }
    }

    render() {
        const { styleElements } = this.props;

        return (
            <Html>
                <Head>
                    <link rel="icon" href="logo.svg"></link>
                    <style dangerouslySetInnerHTML={{ __html: resetStyles }} />
                    {styleElements}
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}

export default MyDocument
