/*
 * @Author: hongbin
 * @Date: 2022-09-09 21:08:20
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-09 21:10:23
 * @Description:关于页
 */
import { FC } from "react";
import styled from "styled-components";
import Navigate from "../src/components/Nav";

interface IProps {}

const About: FC<IProps> = () => {
    return (
        <Container>
            <Navigate />
        </Container>
    );
};

export default About;

const Container = styled.div``;
