/*
 * @Author: hongbin
 * @Date: 2022-08-31 22:36:31
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-16 19:46:39
 * @Description:导航栏
 */
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import styled, { css } from "styled-components";
import { routerConfigure } from "../../router";
import { flexCenter } from "../../styled";
import { phone } from "../../utils/media";
import { Icon, GiteeIcon } from "../Icon";

interface IProps {}

const Navigate: React.FC<IProps> = () => {
    const router = useRouter();

    return (
        <>
            <Head>
                <title>{process.env.NEXT_PUBLIC_TITLE || "宇宙"}</title>
                <meta name="description" content="欢迎来访" />
            </Head>
            <Container>
                {routerConfigure.map(({ path, name, icon }) =>
                    name ? (
                        <Link key={path} href={path}>
                            <Text active={router.pathname === path}>
                                {icon}
                                {name}
                            </Text>
                        </Link>
                    ) : null
                )}
                <Icon
                    link="https://gitee.com/honbingitee/three-dimensional-blog"
                    style={{ position: "absolute", right: "2vh" }}
                >
                    <GiteeIcon />
                </Icon>
            </Container>
        </>
    );
};

export default Navigate;

const Container = styled.nav`
    height: var(--nav-height);
    width: 98vw;
    ${flexCenter};
    /* position: fixed; */
    position: absolute;
    top: 1vh;
    left: 1vw;
    z-index: 999;
    box-shadow: 1vh 0 1vh 0 #165d0a;
    border-radius: 1vmin;
    background: var(--clay-background, #16ae10);
    border-radius: var(--clay-border-radius, 2vh);
    /* box-shadow: var(--clay-shadow-outset, 8px 8px 16px 0 rgba(0, 0, 0, 0.25)),
        inset var(--clay-shadow-inset-primary, -8px -8px 16px 0 rgba(0, 0, 0, 0.25)),
        inset var(--clay-shadow-inset-secondary, 8px 8px 16px 0 hsla(0, 0%, 100%, 0.2)); */
    box-shadow: 1vh 1vh 1vh 0px rgb(33 140 17), inset -1vh -1vh 2vh 0 rgb(21 138 1),
        inset 1vh 1vh 2vh 0 hsl(110deg 50% 50%);
`;

const Text = styled.span<{ active: boolean }>`
    color: #f7f7f7;
    font-weight: bold;
    font-size: 1.5vmax;
    display: inline-block;
    padding: 1vmin 2vmin;
    min-height: 4vmin;
    border-radius: 0.3vh;
    margin: 0 1vh;
    ${flexCenter};

    svg {
        width: 2vh;
        height: 2vh;
        margin-right: 1vh;
    }

    ${({ active }) =>
        active
            ? css`
                  border-radius: 1vh;
                  transition: background 0.3s linear, color 0.3s linear;
                  background: #033a00;
                  color: #3bd61b;
                  text-shadow: 2px 3px 0px #083505;
                  filter: drop-shadow(2px 4px 6px #326c20);
              `
            : css`
                  :hover {
                      background: #4dc537;
                  }
              `};

    ${phone(`
          font-size: 3vmax;

          svg {
            filter: drop-shadow(2px 4px 6px black);
          }
        `)}
`;
