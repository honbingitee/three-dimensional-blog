/*
 * @Author: hongbin
 * @Date: 2022-09-01 10:34:10
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-16 18:51:54
 * @Description:图标入口
 */

import GiteeIcon from "./GiteeIcon";
import PSTreeIcon from "./PSTreeIcon";
import PSDiamond from "./PSDiamond";
import PSArm from "./PSArm";

import styled from "styled-components";
import { memo } from "react";
import { phone } from "../../utils/media";

export { GiteeIcon, PSTreeIcon, PSDiamond, PSArm };

const IconStyled = styled.div`
    width: 5vmin;
    height: 5vmin;
    padding: 1vmin;
    border-radius: 100%;
    transition: background-color 0.3s linear;
    cursor: pointer;

    svg {
        width: 3vmin;
        height: 3vmin;
    }

    :hover {
        background-color: var(--deep-color, #ccc);
    }

    ${phone(`
     width: 5vmax;
    height: 5vmax;
    padding: 1vmax;
        svg {
            width: 3vmax;
            height: 3vmax;
        }
    `)}
`;

interface IIcon {
    children: React.ReactNode;
    link?: string;
    style?: React.CSSProperties;
}

const IconFc: React.FC<IIcon> = ({ children, link, style }) => {
    const props = {
        style,
        onClick: link
            ? () => {
                  window.open(link);
              }
            : undefined,
    };

    return <IconStyled {...props}>{children}</IconStyled>;
};

export const Icon = memo(IconFc);
