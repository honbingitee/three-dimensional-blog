/*
 * @Author: hongbin
 * @Date: 2022-08-31 22:53:05
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-17 15:12:47
 * @Description:醒目的介绍
 */
import { FC, memo } from "react";
import styled, { css } from "styled-components";
import { ILoadInfoControls } from "../../meta/helper/loadInfoControls";
import { fadeIn, FlexDiv } from "../../styled";
import { phone } from "../../utils/media";
import { PSArm, PSDiamond, PSTreeIcon } from "../Icon";

interface IProps {
    loadInfo: {
        info: ILoadInfoControls["info"];
        complete: ILoadInfoControls["complete"];
    };
    enterMeta: () => void;
}

const Banner: FC<IProps> = ({ loadInfo, enterMeta }) => {
    return (
        <Container>
            <DESC>
                <Title color="#62df4c">WELCOME</Title>
                {Object.entries(loadInfo.info).map(([name, value]) => (
                    <TransFlexDiv items="flex-end" space="0.5vh" key={name}>
                        <strong>{name}</strong>
                        <Caption>{value}</Caption>
                    </TransFlexDiv>
                ))}
                <PSTreeIcon />
                <PSDiamond />
                <PSDiamond />
                <PSDiamond />
            </DESC>
            <Main>
                <PSArm />
                <Title>欢迎来到宏斌的3D博客</Title>
                <EnterBtn isShow={loadInfo.complete} onClick={enterMeta}>
                    进入宏斌元宇宙
                </EnterBtn>
            </Main>
        </Container>
    );
};

export default memo(Banner);

const EnterBtn = styled.button<{ isShow: boolean }>`
    width: 20vmax;
    height: 6vmin;
    border: none;
    border-radius: 2vmin;
    font-size: 2.3vmin;
    letter-spacing: 1px;
    font-weight: bold;
    color: #188705;
    background: #6be156;
    box-shadow: 0.5vmin 0.5vmin 0.5vmin 0px #57d047, inset -0.5vmin -0.5vmin 1vmin 0 #42c12ca6,
        inset 0.5vmin 0.5vmin 1vmin 0 #4acb322b;
    /* box-shadow: 0.5vh 0.5vh 0.5vh 0px #57d047, inset -0.5vh -0.5vh 1vh 0 #42c12ca6, inset 0.5vh 0.5vh 1vh 0 #4acb322b; */
    cursor: pointer;
    opacity: ${(props) => +props.isShow};
    visibility: ${(props) => (props.isShow ? "visible" : "hidden")};
    transition: transform 0.3s linear, opacity 0.3s linear;
    z-index: 2;
    :hover {
        transform: translateY(-2px);
    }
    ${phone(`
        height:6vmax;
        font-size: 2.3vmax;
    `)}
`;

const TransFlexDiv = styled(FlexDiv)`
    animation: ${fadeIn} 1s linear;
    font-size: 2vmin;
    ${phone(`
        font-size: 4vmin;
        width: 100%;
    `)}
`;

const Caption = styled.strong`
    font-size: 1.5vmin;
    ${phone(` font-size: 1.5vmax;`)}
`;

const Title = styled.span<{ color?: string }>`
    color: ${(props) => props.color || "#135605"};
    font-weight: bold;
    font-size: 10vmax;
    z-index: 1;
    text-shadow: 0.3vh 0.4vh 0.1vh #4ba13b;
    &:first-child {
        text-shadow: 3px 3px 2px #0e6d0a;
    }
    ${phone(`
        font-size: 17vw;
        height: 20vh;
        &:first-child {
            text-shadow: 3px 3px 2px #0e6d0a;
            height: 25VW;
          font-size: 25vw;
          z-index:-1;
        }
    `)}
`;

const Container = styled.div`
    height: 100vh;
    background: #16ae10;
    display: flex;
    padding: calc(var(--nav-height) + 5vh) 2vw 5vh 2vw;
    overflow: hidden;
    display: flex;
    ${phone(`flex-direction: column;`)}
`;

const DESC = styled.div`
    width: 34vw;
    display: flex;
    flex-direction: column;
    margin-right: 2vw;
    position: relative;
    /* color: #0d4d02; */
    color: #1b760b;
    transition: transform 0.3s linear;

    .PSTreeIcon {
        bottom: 3vh;
        left: 2vw;
        width: 20vw;
        height: 20vw;
        position: absolute;
        transition: transform 0.3s linear;
        filter: drop-shadow(2px 4px 6px #295416);
    }
    .PSDiamond {
        position: absolute;
        width: 3vw;
        height: 3vw;
        min-height: 30px;
        min-width: 30px;
        bottom: 2vh;
        left: 50%;
        transform: translateX(-50%) rotateZ(30deg);
        filter: drop-shadow(2px 4px 6px #295416);
    }
    /* 第二个叫 PSDiamond 的元素 */
    .PSDiamond ~ .PSDiamond {
        left: 60%;
        transform: translateX(-50%) rotateZ(45deg) scale(0.8);
    }
    .PSDiamond ~ .PSDiamond ~ .PSDiamond {
        left: 20%;
        transform: rotateZ(45deg) scale(0.6);
    }

    :hover {
        transform: translateY(-2px);
        .PSTreeIcon {
            transform: translateY(5px);
        }
    }

    ${phone(css`
        flex: 1;
        width: 100%;
        align-items: center;
        transform: translateY(-5vw);
        .PSTreeIcon {
            width: 30vw;
            height: 30vw;
            right: 2vw;
            left: auto;
        }
    `)}
`;

const Main = styled.div`
    /* flex: 1; */
    width: 60vw;
    background: #6be156;
    border-radius: 3vw;
    /* border-bottom-left-radius: 0;
    border-bottom-right-radius: 0; */
    box-shadow: 1vh 1vh 1vh 0px #2ca01c, inset -1vh -1vh 2vh 0 #288218, inset 1vh 1vh 2vh 0 #28a6114d;
    display: flex;
    flex-wrap: wrap;
    position: relative;
    justify-content: center;
    padding: 2vw;
    transition: transform 0.3s linear;

    .PSArm {
        width: 15vw;
        height: 15vw;
        min-height: 200px;
        min-width: 200px;
        position: absolute;
        bottom: 3vh;
        right: 3vw;
        transition: transform 0.3s linear;
        filter: drop-shadow(2px 4px 6px #295416);
    }

    :hover {
        transform: translateY(-2px);
        .PSArm {
            transform: translateY(12px);
        }
    }
    ${phone(css`
        width: 96vw;
        height: 40vh;
    `)}
`;
