/*
 * @Author: hongbin
 * @Date: 2022-09-04 13:06:33
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-04 13:06:34
 * @Description:
 */
declare module "*.png";
declare module "*.jpg";
declare module "*/*.jpg";
