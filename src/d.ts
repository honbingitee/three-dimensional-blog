/*
 * @Author: hongbin
 * @Date: 2022-09-04 13:06:57
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-16 20:34:35
 * @Description:
 */

import { GUI } from "dat.gui";

// import { Object3D as Prev } from "three/src/core/Object3D";

// type A = Omit<Prev, "userData">;
declare module "three/src/core/Object3D" {
    // interface Object3D extends Omit<Prev, "userData"> {
    // interface Object3D<T = { [key: string]: any }> {
    interface Object3D {
        // userData:  T;
        /**
         * 这是一个向库中的类型添加的类型的测试
         */
        hongbin: any;
    }
}

declare global {
    interface Window {
        render: (v?: number) => void;
        renderer: THREE.WebGLRenderer;
        gui: GUI;
        onmousewheel: ((e: { wheelDelta: number }) => void) | null;
        /**
         * 临时获取向量使用
         */
        _vector3: THREE.Vector3;
        acterWrap: THREE.Object3D;
    }
    interface Document {
        onmousewheel: ((e: { wheelDelta: number }) => void) | null;
    }
}
