/*
 * @Author: hongbin
 * @Date: 2022-08-13 14:08:54
 * @LastEditors: hongbin
 * @LastEditTime: 2022-08-20 15:21:09
 * @Description:多个文件都要用的常量
 */

/**
 * 人物相关缩放指数
 * TODO 目前在不改变各参数的情况下人物缩放不能适应现有环境 上坡卡顿多帧检测不到前方物体产生卡顿
 */
export const acterScaleMultiple = 1;
/**
 * 测试模式
 * 开启测试模式 可以一直跳
 */
export const testMode = true;
