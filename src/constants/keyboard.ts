/*
 * @Author: hongbin
 * @Date: 2022-07-21 12:20:09
 * @LastEditors: hongbin
 * @LastEditTime: 2022-07-22 19:47:18
 * @Description:键盘按键配置
 */

interface IKey {
    name: string;
    length?: number;
    mini?: boolean;
    children?: IKey[];
}

/**
 * 按照mac键盘键位设计六行键位
 */
const config: Array<IKey[]> = [
    [
        {
            name: "esc",
            length: 1.5,
        },
        { name: "F1" },
        { name: "F2" },
        { name: "F3" },
        { name: "F4" },
        { name: "F5" },
        { name: "F6" },
        { name: "F7" },
        { name: "F8" },
        { name: "F9" },
        { name: "F10" },
        { name: "F11" },
        { name: "F12" },
        { name: "F13" },
    ],
    [
        { name: "~" },
        { name: "1" },
        { name: "2" },
        { name: "3" },
        { name: "4" },
        { name: "5" },
        { name: "6" },
        { name: "7" },
        { name: "8" },
        { name: "9" },
        { name: "0" },
        { name: "-" },
        { name: "+" },
        { name: "delete", length: 1.5 },
    ],
    [
        { name: "tab", length: 1.5 },
        { name: "Q" },
        { name: "W" },
        { name: "E" },
        { name: "R" },
        { name: "T" },
        { name: "Y" },
        { name: "U" },
        { name: "I" },
        { name: "O" },
        { name: "P" },
        { name: "[" },
        { name: "]" },
        { name: "\\" },
    ],
    [
        { name: "caps lock", length: 1.8 },
        { name: "A" },
        { name: "S" },
        { name: "D" },
        { name: "F" },
        { name: "G" },
        { name: "H" },
        { name: "J" },
        { name: "K" },
        { name: "L" },
        { name: ";" },
        { name: "'" },
        { name: "retutn", length: 1.8 },
    ],
    [
        { name: "shift", length: 2.35 },
        { name: "Z" },
        { name: "X" },
        { name: "C" },
        { name: "V" },
        { name: "B" },
        { name: "N" },
        { name: "M" },
        { name: "<" },
        { name: ">" },
        { name: "/" },
        { name: "shift", length: 2.35 },
    ],
    [
        { name: "fn" },
        { name: "contrl" },
        { name: "option" },
        { name: "command" },
        { name: "space", length: 5.9 },
        { name: "command" },
        { name: "option" },
        { name: "←" },
        {
            name: "↑↓",
            children: [
                { name: "↑", mini: true },
                { name: "↓", mini: true },
            ],
        },
        { name: "→" },
    ],
];
export default config;
// let str = "";
// config.forEach((r) => {
//     r.forEach((k) => {
//         str += k.name;
//     });
// });
// console.log(str);
