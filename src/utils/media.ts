/*
 * @Author: hongbin
 * @Date: 2022-09-08 13:56:01
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-08 19:00:55
 * @Description: styled-components适配不同屏幕尺寸
 */
import { css, FlattenSimpleInterpolation } from "styled-components";

export const mobile = (inner: FlattenSimpleInterpolation) => css`
    @media (max-width: 1000px) {
        ${inner};
    }
`;

export const phone = (inner: FlattenSimpleInterpolation | string) => css`
    @media (max-width: 650px) {
        ${inner};
    }
`;
