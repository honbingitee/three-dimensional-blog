import { createGlobalStyle } from "styled-components";
// import PSFont from "../assets/font/metaVo.ttf";

/* background: ${window.MACOS ? "#345438" : "#025528"}; */
/* src:${() => `url(${PSFont}) format('TrueType')`} */
const GlobalStyle = createGlobalStyle`
  html,body {
    height: 100%;
    }

  body{
    display:flex ;
    flex-direction: column;
  }

  a {
    color: inherit;
    text-decoration: none;
  }

  * {
    box-sizing: border-box;
    padding: 0;
    margin: 0;
    user-select: none;
  }

  :root {
    --primary-color: #16ae10;
    --deep-color:#165d09;
    --nav-height:10vh
  }

  @media (max-width: 650px) {
    :root {
      --nav-height: 20vh;
    }
  }

  ::-webkit-scrollbar-thumb {
    background: var(--deep-color);
    /* border-radius: 4px; */
  }
  ::-webkit-scrollbar {
      width: 4px;
      height: 4px;
      background-color: rgb(38 174 14);
  }

  .dg.ac {
    z-index: 10000 !important;
  }

  @media screen and (orientation: landscape) and (max-height: 550px) {
    * {
      z-index: -1 !important;
    }
    #root::before{
      content: "";
      display: flex;
      width: 100vmax;
      height: 100vmin;
      background: #a0f5a0;
    }
    #root::after{
      content: "为了更好体验，请在浏览器竖屏情况下横持手机浏览";
      color: #448744;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      text-align: center;
      font-size: 5vmin;
      border-radius: 1vmin;
      text-shadow: 0px 1px 7px #fffae5;
      background: linear-gradient(145deg, #abffab, #90dd90);
      box-shadow:  26px 26px 52px #7aba7a,
              -26px -26px 52px #c6ffc6;
    }
  }
`;

export default GlobalStyle;
