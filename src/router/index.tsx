/*
 * @Author: hongbin
 * @Date: 2022-09-01 09:47:46
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-09 21:09:09
 * @Description:路由配置
 */

// import About from "../container/About";
// import Home from "../container/Home";
// import NotFind from "../container/NotFind";
import { AboutIcon, HomeIcon } from "./icon";

export const routerConfigure = [
    {
        path: "/",
        // element: <Home />,
        name: "主页",
        icon: <HomeIcon />,
    },
    {
        path: "/about",
        // element: <About />,
        name: "关于",
        icon: <AboutIcon />,
    },
    {
        path: "*",
        // element: <NotFind />,
    },
];
