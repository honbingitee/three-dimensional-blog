/*
 * @Author: hongbin
 * @Date: 2022-09-04 20:35:56
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-14 19:25:10
 * @Description: 总是报错 有多个THREE实例存在 出此下策
 */

import * as THREE from "three";
import { testMode } from "../constants";

export default THREE;

/**
 * 喷射器相关参数
 */
export const ejectorParams = {
    on: false,
    allowJumpCount: testMode ? 100 : 3,
    currentJumpCount: 0,
    isCanJump: function () {
        if (!this.on) return testMode;
        this.count();
        return this.currentJumpCount !== (this.allowJumpCount ^ 1);
    },
    recount: function () {
        this.currentJumpCount = 0;
    },
    count: function () {
        this.currentJumpCount = (this.currentJumpCount + 1) % this.allowJumpCount;
    },
    open: function () {
        this.on = true;
    },
};

/**
 * 碰撞检测相关参数
 */
export const collideParams = {
    /**
     * 检测附近距离的物体
     */
    checkDistance: 80,
    /**
     * home星球地壳高度
     */
    homeFloorHeight: 80,
    homeFloorY: -1841.7551062629302,
};

/**
 * 设置新的碰撞检测参数
 */
export const setCollideParams = (params: Partial<typeof collideParams>) => {
    Object.assign(collideParams, params);
};
