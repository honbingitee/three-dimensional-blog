/*
 * @Author: hongbin
 * @Date: 2022-09-12 08:11:48
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-14 19:49:08
 * @Description:宇宙中的物体 在星球之外
 */
import THREE from "../../";
import { animationControl } from "../../helper";
import { acterWrap, setActerPosition } from "../../object";
import { toOtherPlanet } from "../../script";
import { MonitorArray } from "../../types";
import { guiTestPosition } from "../../utils/gui";
import { TextToast } from "../Forest/TextToast";
import { loadGltf } from "./../../helper/loaderHelper";

const withVolume: MonitorArray<THREE.Object3D> = [];
const objects = new THREE.Group();

withVolume.push = (...items: typeof withVolume[number][]) => {
    if (typeof withVolume.monitoringChanges === "function") withVolume.monitoringChanges(...items);
    return Array.prototype.push.call(withVolume, ...items);
};

/**
 * 在userDate上定义人物进入物体触发方法
 */
const enterObjectTrigger = (object: THREE.Object3D) => {
    const EnterToast = TextToast({ text: "F 离开当前宇宙" });

    EnterToast.init(objects, () => {
        console.log("按下了f键 离开当前宇宙");
        const _vec = acterWrap.position.clone();
        toOtherPlanet("home", () => {
            const dir = acterWrap.position.x > 550;
            const x = dir ? 2 : -2;
            const y = dir ? 3 : 0.3;
            animationControl.add({
                part: 60,
                count: 0,
                callback: function () {
                    _vec.x -= x;
                    _vec.y += y;
                    setActerPosition(_vec);
                    this.count++;
                },
                complete: () => {},
            });
        });
    });

    window.addEventListener("startrender", () => {
        object.userData.box3.setFromObject(object);
    });

    object.userData = {
        box3: new THREE.Box3().setFromObject(object),
        nearbyCheck: (vector: THREE.Vector3) => {
            const isIn = object.userData.box3.containsPoint(vector);
            //进入物体范围
            if (isIn) {
                //已经显示了不重复执行
                if (!EnterToast.toast.userData.isShow) {
                    //确定方向关系 决定提示文字出现位置
                    // 550 是home星球的半径 大于正向的半径表示人物在home外
                    if (acterWrap.position.x > 550) {
                        //forest星球
                        EnterToast.toast.rotation.y = Math.PI / 2;
                        EnterToast.toast.position.set(576, 18, -18);
                    } else {
                        //home星球
                        EnterToast.toast.rotation.y = -Math.PI / 2;
                        EnterToast.toast.position.set(520, 24, -27);
                    }
                    EnterToast.toast.userData.show();
                }
            } else EnterToast.toast.userData.hidden();
        },
    };
};

const loadPortal = () => {
    loadGltf("/api/glb?name=portal.glb")
        .then((gltf) => {
            console.log(" ", gltf.scene);
            objects.add(gltf.scene);
            withVolume.push(gltf.scene);
            gltf.scene.position.set(550, 10, 15);
            gltf.scene.rotateY(Math.PI / 2);
            guiTestPosition(gltf.scene, 1000);
            const portal = gltf.scene.getObjectByName("portal");
            if (portal) enterObjectTrigger(portal);
        })
        .catch((err) => {
            console.log("err", err);
        });
};

export const universe = {
    init: () => {
        loadPortal();
    },
    withVolume,
    objects,
};
