/*
 * @Author: hongbin
 * @Date: 2022-09-07 14:18:28
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-07 14:20:48
 * @Description: 布道砖
 */
import THREE from "../../";
import { getSize, modelSurround } from "../../helper";
import { loadGltf } from "../../helper/loaderHelper";

export const loadBlocks = (objects: THREE.Object3D, floor: THREE.Object3D) => {
    /**
     * 砖块模型加载
     */
    loadGltf("api/glb?name=blocks.glb")
        .then((gltf) => {
            //存放根据提供的砖块模型组成的道路 不加入检测 减小开销 砖块包裹一个透明的容器中 只检测外部这个盒子 防止行走不顺畅 掉进砖缝中
            /**
             * 获得三块砖的模型
             */
            const one = gltf.scene.getObjectByName("1");
            const two = gltf.scene.getObjectByName("2");
            const three = gltf.scene.getObjectByName("3");
            if (!one || !two || !three) return;
            const provider = [one, two, three];
            const { length: count } = provider;

            const space = getSize(one).z + 1;
            /**
             * 一行随机生成的砖块
             */
            const randomGenBlocksRow = () => {
                const row = new THREE.Group();
                for (let i = 0; i < count; i++) {
                    //随机获取得一个模型
                    const model = provider[Math.floor(Math.random() * count)].clone();
                    row.add(model);
                    //这样 0，1，2 是 0  3，4，5 是 1 有序排列保持坐标一致
                    const column = Math.floor(i / count);

                    const x = column * space;
                    const y = 0;
                    const z = (i % count) * space;
                    model.position.set(x, y, z);
                }
                return row;
            };

            const blocksGroup = modelSurround({
                model: randomGenBlocksRow,
                count: 30, //一共30行
                radius: 460 * 4, //半径和地面一样大
                radian: 6, //只生成6度的砖道
                syncRotation: true,
                column: 4, //生成4列
            });

            // blocksGroup.position.set(0, -1839.45, 0);
            //高度由地面的位移和地面贴图的凸起高度决定
            blocksGroup.position.set(0, floor.position.y + 2.5, 0);

            objects.add(blocksGroup);
            window.render();
        })
        .catch((err) => {
            console.log("err", err);
        });
};
