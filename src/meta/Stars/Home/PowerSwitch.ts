/*
 * @Author: hongbin
 * @Date: 2022-07-28 08:05:25
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-12 13:51:03
 * @Description:电脑电源键提示
 */

import * as THREE from "three";
import { animationControl } from "../../helper";
import { createText } from "../../helper/loaderHelper";

function PowerSwitch() {
    let handleChange: (off: boolean) => void;
    const toastMaterial = new THREE.MeshBasicMaterial({
        color: 0xffffff,
        side: THREE.DoubleSide,
        transparent: true,
        opacity: 0,
    });
    const onText = createText("F 开机", {
        size: 4,
        height: 0.2,
        curveSegments: 0.02,
        bevelThickness: 0.02,
        bevelSize: 0.01,
    });
    const offText = createText("F 关机", {
        size: 4,
        height: 0.2,
        curveSegments: 0.02,
        bevelThickness: 0.02,
        bevelSize: 0.01,
    });
    onText.material = offText.material = toastMaterial;
    const toast = genToast();

    function init(model: THREE.Object3D, onChange: typeof handleChange) {
        /**
         * 在开关旁边创建一个操作提示
         */
        model.getWorldPosition(toast.position);
        toast.position.y -= 80 - 9;
        toast.position.x += 10;
        toast.position.z -= 5;
        handleChange = onChange;
    }

    /**
     * 切换提示文字
     */
    function toggleToastText(isOff: boolean) {
        toast.children = [];
        toast.add(isOff ? onText : offText);
    }

    function pressKey(e: KeyboardEvent) {
        if (e.key === "f") {
            const isOff = !toast.userData["off"];
            handleChange(isOff);
            toggleToastText(isOff);
        }
    }

    function genToast() {
        const geometry = new THREE.RingGeometry(4, 5, 4);
        geometry.rotateZ(-Math.PI / 4);
        geometry.translate(1, 2, 0);

        const mesh = new THREE.Mesh(geometry, toastMaterial);

        const userData = {
            //关机状态
            off: true,
            //显示提示文字
            isShow: false,
            //切换显示提示文字
            toggle: (state: boolean) => {
                userData.isShow = state;
                if (toastMaterial.opacity >= 1 && state) return;

                toggleToastText(userData.off);

                animationControl.add({
                    part: 10,
                    count: 0,
                    callback: function () {
                        toastMaterial.opacity += 0.1 * (state ? 1 : -1);
                        this.count++;
                    },
                });
                if (state) {
                    window.addEventListener("keypress", pressKey);
                } else window.removeEventListener("keypress", pressKey);
            },
            show: () => {
                if (!userData.isShow) userData.toggle(true);
            },
            hidden: () => {
                if (userData.isShow) userData.toggle(false);
            },
        };
        mesh.userData = userData;
        return mesh;
    }

    return { init, toast };
}

export const powerSwitchController = PowerSwitch();
