/*
 * @Author: hongbin
 * @Date: 2022-09-07 14:21:58
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-12 13:49:22
 * @Description:
 */
import THREE from "../../";
import { getSize } from "../../helper";
import { loadGltf, textureLoader } from "../../helper/loaderHelper";
import kmycImg from "../../../assets/img/kmyc.png";
import websiteImg from "../../../assets/img/website.png";
import csdnImg from "../../../assets/img/csdn.png";
import giteeImg from "../../../assets/img/gitee.png";

const propagandas = [
    {
        link: "https:kmyc.hongbin.xyz",
        img: kmycImg.src,
        name: "3D抗美援朝数据可视化-three.js",
    },
    {
        link: "https:hongbin.xyz",
        img: websiteImg.src,
        name: "宏斌的博客-next.js",
    },
    {
        link: "https://blog.csdn.net/printf_hello",
        img: csdnImg.src,
        name: "我的csdn",
    },
    {
        link: "https:gitee.com",
        img: giteeImg.src,
        name: "我的gitee",
    },
];

/**
 * 宣传展示牌
 */
export const loadPropaganda = (objects: THREE.Object3D, withVolume: THREE.Object3D[], floorHeight: number) => {
    loadGltf("api/glb?name=propaganda.glb")
        .then((gltf) => {
            const collection = gltf.scene;
            const { x } = getSize(collection);

            propagandas.forEach(({ link, name, img }, index) => {
                const collectionModel = collection.clone();
                const propaganda = collectionModel.getObjectByName("propaganda") as THREE.Mesh;
                collectionModel.position.set(-200, -10, -x * 0.9 * (index - propagandas.length / 2));
                collectionModel.rotation.y = Math.PI / -2.4;
                objects.add(collectionModel);
                withVolume.push(collectionModel);
                textureLoader.load(img, (texture) => {
                    const { z, y } = getSize(propaganda);
                    texture.encoding = THREE.sRGBEncoding;
                    const mesh = new THREE.Mesh(
                        new THREE.PlaneGeometry(z, y),
                        new THREE.MeshBasicMaterial({
                            map: texture,
                        })
                    );
                    propaganda.getWorldPosition(mesh.position);
                    mesh.rotation.y = collectionModel.rotation.y + Math.PI;
                    objects.add(mesh);
                    mesh.position.y -= floorHeight;
                    mesh.position.x += 0.5;
                    collectionModel.remove(propaganda);
                });
            });
        })
        .catch((err) => {
            console.log("加载出错", err);
        });
};
