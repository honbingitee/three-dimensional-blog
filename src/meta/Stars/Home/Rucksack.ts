/*
 * @Author: hongbin
 * @Date: 2022-09-07 15:18:54
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-07 15:21:21
 * @Description:背包
 */
import THREE from "../../";
import { loadGltf } from "../../helper/loaderHelper";
import { roughnessParams } from "./Glass";

const glassMaterial = new THREE.MeshPhysicalMaterial({
    side: THREE.DoubleSide,
    specularColor: new THREE.Color("#ffffff"),
    ...roughnessParams,
});

export const loadRucksack = (objects: THREE.Object3D) => {
    loadGltf("api/glb?name=rucksack.glb")
        .then(({ scene }) => {
            // rucksack
            const rucksack = scene.getObjectByName("rucksack") as THREE.Mesh;
            const rucksackLib = scene.getObjectByName("rucksack-lib") as THREE.Mesh;
            if (!rucksack || !rucksackLib) return;
            rucksack.material = glassMaterial;
            rucksackLib.material = new THREE.MeshPhysicalMaterial({
                side: THREE.DoubleSide,
                ...roughnessParams,
                specularColor: new THREE.Color("#5511ff"),
                metalness: 0.5,
            });

            scene.position.set(-10, 135, 98);
            objects.add(scene);
        })
        .catch((err) => {
            console.log("err", err);
        });
};
