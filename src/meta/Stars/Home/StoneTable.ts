/*
 * @Author: hongbin
 * @Date: 2022-09-07 14:11:45
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-07 14:16:54
 * @Description:石碑(功德碑)
 */
import THREE from "../../";
import { createText, loadGltf } from "../../helper/loaderHelper";

/**
 * 石碑
 */
export function stoneTabletLog(objects: THREE.Object3D) {
    const group = new THREE.Group();
    loadGltf("api/glb?name=stone-tablet.glb")
        .then((gltf) => {
            const tablet = gltf.scene.children[0] as THREE.Mesh;
            tablet.material = new THREE.MeshPhongMaterial({
                color: 0x1e1f27,
                flatShading: true,
                side: THREE.DoubleSide,
            });

            const log = createText(
                `
                本星球创于
                公元2022年7月11日
                由刘宏斌
                一手打造 
                耗资巨大
                功德无量
                
                
                
                
                
                         hongbin`,
                {
                    size: 0.7,
                    height: 0.1,
                    curveSegments: 0.02,
                    bevelThickness: 0.02,
                    bevelSize: 0.01,
                },
                { forward: 0x333333, side: 0x222222 }
            );
            log.position.set(-44, 18, 113);
            const title = createText(
                "功德碑",
                {
                    size: 1,
                    height: 0.2,
                    curveSegments: 0.1,
                    bevelThickness: 0.1,
                    bevelSize: 0.05,
                },
                { forward: "#111111", side: "#222222" }
            );
            title.position.set(-2.5, 15, 2.4);
            group.add(tablet, title);
            //=> 不检测log 开销太大 几十万个点检测会立刻掉帧 坏处就是无法碰撞检测 但影响不大
            objects.add(log);
            log.rotateX(-Math.PI / 2.3);
            log.scale.set(6, 6, 6);
            window.render();
        })
        .catch((err) => {
            console.log("err", err);
        });

    group.position.set(30, -10, 180);
    group.rotateX(-Math.PI / 2.3);
    group.scale.set(6, 6, 6);

    return group;
}
