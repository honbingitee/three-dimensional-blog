/*
 * @Author: hongbin
 * @Date: 2022-09-05 15:43:02
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-14 19:26:20
 * @Description:真假地面
 */
import THREE, { collideParams } from "../../";
import { textureLoader } from "../../helper/loaderHelper";
import river_small_rocks_diff_1k from "../../../assets/textures/river/river_small_rocks_diff_1k.jpg";
import river_small_rocks_nor_gl_1k from "../../../assets/textures/river/river_small_rocks_nor_gl_1k.jpg";
import river_small_rocks_ao_1k from "../../../assets/textures/river/river_small_rocks_ao_1k.jpg";
import river_small_rocks_disp_1k from "../../../assets/textures/river/river_small_rocks_disp_1k.jpg";

const { DoubleSide, Mesh, MeshPhongMaterial } = THREE;

/**
 * 初始化地表
 * 分为表面带纹理的地表 和紧贴下方的实际检测弧形地面
 * displacement纹理检测不准确 所以下方贴一个弧形几何作为真实地面
 */
export const initFloor = () => {
    /**
     * 地面
     */
    const floorGeometry = new THREE.SphereGeometry(460, 50, 50, 0, 2 * Math.PI, 0, 0.3);
    const textureRepeat = (texture: THREE.Texture) => {
        // texture.encoding = sRGBEncoding;
        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
        texture.offset.set(0, 0);
        texture.repeat.set(5, 5);
    };

    const coastSandRocksTexture = {
        map: textureLoader.load(river_small_rocks_diff_1k.src, textureRepeat),
        normalMap: textureLoader.load(river_small_rocks_nor_gl_1k.src, textureRepeat),
        aoMap: textureLoader.load(river_small_rocks_ao_1k.src, textureRepeat),
        displacementMap: textureLoader.load(river_small_rocks_disp_1k.src, textureRepeat),
        displacementScale: 1,
    };
    const material = new THREE.MeshPhongMaterial({
        color: 0x696456,
        ...coastSandRocksTexture,
        side: DoubleSide,
    });
    const floor = new Mesh(floorGeometry, material);
    // floor.rotateX(-Math.PI / 2);
    floor.position.y = collideParams.homeFloorY;
    floor.scale.set(4, 4, 4);

    const realFloor = new Mesh(
        floorGeometry,
        new MeshPhongMaterial({
            color: 0x2e2925,
            side: DoubleSide,
        })
    );
    realFloor.position.y = collideParams.homeFloorY + 1.7;
    realFloor.scale.set(4, 4, 4);
    realFloor.userData.uphillHeight = 0.2;

    return {
        floor,
        realFloor,
    };
};
