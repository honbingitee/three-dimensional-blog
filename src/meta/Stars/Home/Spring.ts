/*
 * @Author: hongbin
 * @Date: 2022-09-07 15:16:39
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-07 15:18:29
 * @Description:弹簧
 */
import THREE from "../../";
import { acterScaleMultiple } from "../../../constants";
import { animationControl, intervalObj, transparentModel } from "../../helper";
import { loadGltf } from "../../helper/loaderHelper";
import { acterJump } from "../../script";

export const loadSpring = (objects: THREE.Object3D, withVolume: THREE.Object3D[]) => {
    loadGltf("api/glb?name=spring.glb")
        .then((gltf) => {
            const root = gltf.scene;
            const spring = root.getObjectByName("spring");
            const wrap = root.getObjectByName("wrap") as THREE.Mesh;
            const plate = root.getObjectByName("plate");
            if (!plate || !wrap || !spring) return;
            spring.position.set(-34, -2, 22);
            wrap.position.set(-34, 3.8, 22);
            plate.position.set(-34, 8.8, 22);

            const { y } = spring.scale;
            const springScaleHeight = 10;

            transparentModel(wrap);

            wrap.userData = intervalObj({
                effect: function () {
                    animationControl.add({
                        part: 30,
                        count: 0,
                        callback: function () {
                            const dir = (this.count < this.part / 2 ? -1 : 1) * this.part;

                            spring.scale.y += y / dir;
                            plate.position.y += springScaleHeight / dir;
                            wrap.position.y += springScaleHeight / dir;

                            this.count++;
                            if (this.count === this.part / 2) {
                                acterJump(400 * acterScaleMultiple);
                            }
                        },
                    });
                },
            });
            // 只检测外壳即可
            withVolume.push(wrap);
            objects.add(spring, wrap, plate);
            window.render();
        })
        .catch((err) => {
            console.log("err", err);
        });
};
