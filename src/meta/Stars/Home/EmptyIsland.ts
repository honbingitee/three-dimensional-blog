/*
 * @Author: hongbin
 * @Date: 2022-09-07 14:31:58
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-07 15:13:59
 * @Description:空中岛屿
 */
import THREE from "../../";
import { animationControl, intervalObj, transparentModel } from "../../helper";
import { loadGltf } from "../../helper/loaderHelper";
import { acterWrap } from "../../object";
import { PickUpController } from "../Forest/TextToast";
import { roughnessParams } from "./Glass";

export const loadEmptyIsland = (objects: THREE.Object3D, withVolume: THREE.Object3D[]) => {
    loadGltf("api/glb?name=emptyIsland.glb")
        .then((gltf) => {
            const root = gltf.scene;
            const plane = root.getObjectByName("plane") as THREE.Mesh;
            const island = root.getObjectByName("island") as THREE.Mesh;
            island.material = new THREE.MeshPhysicalMaterial({
                side: THREE.DoubleSide,
                ...roughnessParams,
                specularColor: new THREE.Color("#5511ff"),
            });

            root.position.set(0, 135, 100);

            withVolume.push(root);
            objects.add(root);

            const material = island.material as THREE.MeshPhysicalMaterial;
            transparentModel(plane);

            PickUpController.init(objects, () => {
                console.log("按下了f键 拾起🎒");
                const rucksack = objects.getObjectByName("rucksack") as THREE.Mesh;
                const rucksackLib = objects.getObjectByName("rucksack-lib") as THREE.Mesh;
                acterWrap.add(rucksack, rucksackLib);
                plane.userData = {};
            });
            PickUpController.toast.rotation.x = -Math.PI / 2;
            PickUpController.toast.rotation.z = Math.PI;
            PickUpController.toast.position.set(1, 137.5, 99);
            PickUpController.toast.scale.set(1.5, 1.5, 1.5);

            plane.userData = intervalObj(
                {
                    timer: null,
                    effect: function () {
                        PickUpController.toast.userData.show();
                        clearTimeout(plane.userData.timer);
                        plane.userData.timer = setTimeout(() => {
                            PickUpController.toast.userData.hidden();
                        }, 510);
                    },
                },
                500
            );
            island.userData = {
                allow: true,
                //直执行一次
                effect: function () {
                    this.allow = false;
                    animationControl.add({
                        part: 30,
                        count: 0,
                        callback: function () {
                            material.metalness += 0.033;
                            this.count++;
                        },
                    });
                },
            };

            window.render();
        })
        .catch((err) => {
            console.log("err", err);
        });
};
