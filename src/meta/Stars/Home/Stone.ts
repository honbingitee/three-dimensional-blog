/*
 * @Author: hongbin
 * @Date: 2022-09-05 17:07:54
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-05 20:27:39
 * @Description:石头
 */
import THREE from "../../";
// import stoneModel from "../../../assets/model/stone.glb";
import { loadGltf, textureLoader } from "../../helper/loaderHelper";
import rock_pitted_mossy_diff_1k from "../../../assets/textures/rock/rock_pitted_mossy_diff_1k.jpg";
import rock_pitted_mossy_nor_gl_1k from "../../../assets/textures/rock/rock_pitted_mossy_nor_gl_1k.jpg";
import rock_pitted_mossy_ao_1k from "../../../assets/textures/rock/rock_pitted_mossy_ao_1k.jpg";

export const initStone = (callback: (stone: THREE.Object3D) => void) => {
  const textureRepeat = (texture: THREE.Texture) => {
    // texture.encoding = sRGBEncoding;
    texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
    texture.offset.set(0, 0);
    texture.repeat.set(5, 5);
  };

  const rockPittedTexture = {
    map: textureLoader.load(rock_pitted_mossy_diff_1k.src, textureRepeat),
    normalMap: textureLoader.load(
      rock_pitted_mossy_nor_gl_1k.src,
      textureRepeat
    ),
    aoMap: textureLoader.load(rock_pitted_mossy_ao_1k.src, textureRepeat),
  };

  loadGltf("/api/glb?name=stone.glb")
    .then(gltf => {
      const stone = gltf.scene.children[0] as THREE.Mesh;
      const material = new THREE.MeshPhongMaterial({
        color: 0x696456,
        ...rockPittedTexture,
        side: THREE.DoubleSide,
      });
      stone.material = material;
      stone.position.set(100, 8, 90);
      stone.scale.set(10, 10, 10);
      callback(stone);
    })
    .catch(err => {
      console.log("err", err);
    });
};
