/*
 * @Author: hongbin
 * @Date: 2022-09-07 09:27:40
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-10 16:31:31
 * @Description:宝箱
 */
import { treasureChestGroup } from ".";
import THREE, { ejectorParams } from "../../";
import { animationControl, destroyObject, intervalObj, transparentModel } from "../../helper";
import { acterWrap, cameraWrap } from "../../object";
import { TextToast } from "./TextToast";

/**
 * 喷射器宝箱
 */
export function createTreasureChest(objects: THREE.Object3D, withVolume: THREE.Object3D[]) {
    // 添加物体
    const treasureChestInstance = treasureChestGroup.clone(true);
    objects.add(treasureChestInstance);
    treasureChestInstance.position.set(27, 15, -12);
    treasureChestInstance.rotateY(Math.PI / 2);
    withVolume.push(treasureChestInstance);
    //添加踩下事件
    const cover = treasureChestInstance.getObjectByName("treasureChestCover")!;
    const floor = treasureChestInstance.getObjectByName("floorEffect")!;
    const openToast = TextToast({ text: "F 打开", translate: new THREE.Vector3(17, 0, 0) });
    openToast.toast.position.set(26, 18, -12);
    openToast.toast.rotateY(-Math.PI / 2);

    let isOpen = false;
    let isOpening = false;
    openToast.init(objects, () => {
        if (isOpening) return;
        console.log(`按下了f键`);
        //开箱
        if (!isOpen) {
            isOpening = true;
            animationControl.add({
                part: 40,
                count: 0,
                callback: function () {
                    cover.rotation.x += 0.005;
                    this.count++;
                },
                complete: () => {
                    setTimeout(() => {
                        animationControl.add({
                            part: 60,
                            count: 0,
                            callback: function () {
                                cover.rotation.x += 0.038;
                                this.count++;
                            },
                            complete: () => {
                                isOpen = true;
                                isOpening = false;
                                openToast.toast.userData.updateText("F 拾取");
                            },
                        });
                    }, 50);
                },
            });
            return;
        }
        //拾取
        const ejector = objects.getObjectByName("ejector") as THREE.Mesh;
        if (!ejector) throw Error("未获取到喷射器对象");
        ejector.userData.clearState();
        ejectorParams.open();
        acterWrap.add(ejector);
        destroyObject(openToast.toast, objects);
        floor.userData = {};
    });

    const _euler = new THREE.Euler(0, 0, 0, "YXZ");
    const maxPolarAngle = Math.PI / 2;
    const minPolarAngle = Math.PI / 2;

    floor.userData = intervalObj(
        {
            timer: null,
            effect: () => {
                openToast.toast.userData.show();
                _euler.setFromQuaternion(cameraWrap.quaternion);
                _euler.x = Math.max(Math.PI / 2 - maxPolarAngle, Math.min(Math.PI / 2 - minPolarAngle, _euler.x));
                openToast.toast.quaternion.setFromEuler(_euler);

                clearTimeout(floor.userData.timer);
                floor.userData.timer = setTimeout(() => {
                    openToast.toast.userData.hidden();
                }, 310);
            },
        },
        300
    );
}
