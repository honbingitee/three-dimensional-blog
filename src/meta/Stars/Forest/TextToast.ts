/*
 * @Author: hongbin
 * @Date: 2022-08-12 19:47:34
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-14 07:57:03
 * @Description:拾取物资提示
 */
import THREE from "../../";
import { animationControl, destroyObject } from "../../helper";
import { createText } from "../../helper/loaderHelper";

const { DoubleSide, Group, Mesh, MeshBasicMaterial, Object3D, RingGeometry } = THREE;

interface Params {
    color?: string;
    text?: string;
    translate?: THREE.Vector3;
}

export function TextToast(params?: Params) {
    let handlePressKey: () => void;
    const toastMaterial = new MeshBasicMaterial({
        color: params?.color ?? 0xffffff,
        side: DoubleSide,
        transparent: true,
        opacity: 0,
    });

    // gui.addColor({ color: "#ccc" }, "color").onChange((v) => {
    //     toastMaterial.color = new Color(v);
    //     window.render();
    // });
    const handleCreatText = (text: string) => {
        const textMesh = createText(text, {
            size: 4,
            height: 0.2,
            curveSegments: 0.02,
            bevelThickness: 0.02,
            bevelSize: 0.01,
        });
        textMesh.material = toastMaterial;
        return textMesh;
    };

    let textToast = handleCreatText(params?.text ?? "F 拾取");

    const toast = genToast();

    function init(parent: THREE.Object3D, callback: typeof handlePressKey) {
        /**
         * 在开关旁边创建一个操作提示
         */
        // model.getWorldPosition(toast.position);
        // toast.position.y -= 60 - 9;
        // toast.position.x += 10;
        // toast.position.z -= 5;
        parent.add(toast);
        handlePressKey = callback;
    }

    function pressKey(e: KeyboardEvent) {
        if (e.key === "f") {
            handlePressKey();
        }
    }

    function genToast() {
        const geometry = new RingGeometry(4, 5, 4);
        geometry.rotateZ(-Math.PI / 4);
        geometry.translate(1, 2, 0);

        const mesh = new Mesh(geometry, toastMaterial);
        mesh.add(textToast);

        const userData = {
            //显示提示文字
            isShow: false,
            //切换显示提示文字
            toggle: (state: boolean) => {
                userData.isShow = state;
                if (toastMaterial.opacity >= 1 && state) return;

                animationControl.add({
                    part: 10,
                    count: 0,
                    callback: function () {
                        toastMaterial.opacity += 0.1 * (state ? 1 : -1);
                        this.count++;
                    },
                });
                if (state) {
                    window.addEventListener("keypress", pressKey);
                } else window.removeEventListener("keypress", pressKey);
            },
            show: () => {
                if (!userData.isShow) userData.toggle(true);
            },
            hidden: () => {
                if (userData.isShow) userData.toggle(false);
            },
            updateText: (text: string) => {
                destroyObject(textToast, mesh);
                textToast = handleCreatText(text);
                mesh.add(textToast);
            },
        };
        //套壳可以按照物体中心旋转
        const group = new Group();
        group.userData = userData;
        group.add(mesh);
        //设置旋转半径
        params?.translate && mesh.position.copy(params.translate);
        return group;
    }

    return { init, toast };
}

export const PickUpController = TextToast();
