/*
 * @Author: hongbin
 * @Date: 2022-09-04 11:43:20
 * @LastEditors: hongbin
 * @LastEditTime: 2022-09-17 23:32:00
 * @Description: three操作核心逻辑
 */
import THREE, { ejectorParams } from ".";
import { acterScaleMultiple } from "../constants";
import {
    ActerInWherePlanet,
    animationControl,
    AnimationPlayer,
    dirType,
    getSize,
    nearCheck,
    scaleToTarget,
    stats,
    uphillCheck,
} from "./helper";
import HControls from "./myControls";
import {
    acterWrap,
    cameraWrap,
    hemisphereLight,
    initScene,
    initWebGLRenderer,
    intGlobalVariable,
    scene,
} from "./object";
import HOME from "./Stars/Home";
import FOREST, { handleParseGrove, handleParsePlain, handleParseTreasureChest } from "./Stars/Forest";
import { parseModel } from "./utils/worker";
import { directionCheck, fallCollide, upCollide } from "./utils/RayControl";
import { _undergroundMesh } from "./worker/helper/undergroundCheck";
import { loadInfoControls } from "./helper/loadInfoControls";
import { universe } from "./Stars/Universe";

let controls: ReturnType<typeof HControls>;
const objects: Array<THREE.Object3D> = [];

let rightDistance: number;
let forwardDistance: number;
/**
 * 正在上的坡的斜度
 */
let hillAngle = 1;
let moveForward = false;
let moveBackward = false;
let moveLeft = false;
let moveRight = false;
/**
 * iframe id
 */
let animationFrame: number;
/**
 * 按下shift键加速
 */
let pressShift = false;
let canJump = false;
let prevTime = 0;
const direction = new THREE.Vector3();
const velocity = new THREE.Vector3();
const planets = [HOME, FOREST];
/**
 * 跳跃高度
 */
const JumpHeight = 200 * acterScaleMultiple * acterScaleMultiple;
/**
 * 第一次按下键盘
 */
let enterKey = false;
/**
 * 移动中
 */
let isMoving = false;

let acterHeight = 0;
/**
 * 附近的物体 减少射线检测开销 只检测附近的物体
 */
// let nearbyObjects: THREE.Object3D[] = [];
let isUphill = false;
let isFallIntoFloor = false;
const acterPlayer = AnimationPlayer();

/**
 * 当前所在的星球
 */
let currentPlanet: typeof planets[number] = planets[0];

let myWorker: Worker;

const initWebWorker = () => {
    myWorker = new Worker(new URL("../meta/worker/script.ts", import.meta.url));
    myWorker.onmessage = function (e) {
        const { work_type } = e.data;
        switch (work_type) {
            case "calculate":
                hemisphereLight.intensity = e.data.intensity;

                isFallIntoFloor = e.data.isFallIntoFloor;
                break;
            case "parseModel":
                // console.log("接收到解析的模型结构:", e.data);
                const object = parseModel(e.data);

                switch (e.data.modelName) {
                    case "acter.glb":
                        acterWrap.add(object);
                        acterPlayer.init(object, object.animations);
                        // console.warn("人物添加：", object, acterWrap);
                        break;
                    case "plain.glb":
                        handleParsePlain(object);
                        break;
                    case "grove.glb":
                        handleParseGrove(object);
                        break;
                    case "treasureChest.glb":
                        handleParseTreasureChest(object);
                        break;
                    default:
                        throw new Error("未设置的接收程序的模型");
                }
                break;
            case "model_load_start":
                loadInfoControls.addInfo("Web Worker 初始化", "完成");
                break;
            case "model_load":
                loadInfoControls.addInfo("Web Worker 模型加载", e.data.progress);
                break;
            case "model_load_complete":
                loadInfoControls.addInfo("Web Worker 加载", "完成");
                break;
        }
    };
    myWorker.onerror = (err) => {
        console.error("work出错：", err, err.message);
    };

    myWorker.postMessage({ work_type: "test_connect" });
    myWorker.postMessage({ work_type: "modelParse", name: "plain.glb" });
    myWorker.postMessage({ work_type: "modelParse", name: "acter.glb" });
    myWorker.postMessage({ work_type: "modelParse", name: "grove.glb" });
    myWorker.postMessage({ work_type: "modelParse", name: "treasureChest.glb" });
};

/**
 * 初始化镜头控制器
 * 指针锁定控制器 https://threejs.org/docs/index.html?q=cont#examples/zh/controls/PointerLockControls
 * 锁定鼠标api https://developer.mozilla.org/zh-CN/docs/Web/API/Pointer_Lock_API
 */
function initControls() {
    controls = HControls(cameraWrap, document.body, acterWrap);

    // document.addEventListener("click", (e) => {
    //     const clickDom = e.target as HTMLElement;
    //     if (clickDom.nodeName === "CANVAS") {
    //         // if (
    //         //     clickDom.nodeName === "CANVAS" &&
    //         //     clickDom.getAttribute("data-engine") &&
    //         //     clickDom.getAttribute("data-engine")?.indexOf("three.js") != -1
    //         // ) {
    //         controls.lock();
    //         acterHeight = getSize(acterWrap).y;
    //     }
    // });
    controls.addEventListener("lock", function () {
        //由于指针锁定控制器在释放鼠标后停止计时 下次进入会积压太长时间 导致delta过大相关变化直接影响 比如直接向下位置数千高度导致人物在空中直接到地下
        prevTime = performance.now();
        acterInWherePlanet.updatePlanets();
        /**
         * 更新forest星球地下物体位置 用来box3检测
         */
        const plainUndergroundVec = new THREE.Vector3();
        _undergroundMesh.getWorldPosition(plainUndergroundVec);

        myWorker.postMessage({
            work_type: "start",
            undergroundParams: {
                forest: { position: plainUndergroundVec },
            },
        });
        animate();
        window.dispatchEvent(new CustomEvent("startrender"));
    });
    controls.addEventListener("unlock", function () {
        cancelAnimationFrame(animationFrame);
        window.dispatchEvent(new CustomEvent("stoprender"));
    });

    return {
        /**
         * 锁定鼠标 控制器接管
         */
        lockMouse: () => {
            controls.lock();
            acterHeight = getSize(acterWrap).y;
        },
        removeEventListener: controls.removeEventListener,
        unlockCallback: (callback: () => void) => {
            controls.addEventListener("unlock", function () {
                cancelAnimationFrame(animationFrame);
                window.dispatchEvent(new CustomEvent("stoprender"));
                callback();
            });
        },
    };
}

/**
 * 按键监听 前后左右跳 等操作
 */
function initKeyPressListen() {
    const handleDown = () => {
        isMoving = true;
        acterPlayer.start();
        console.log("pres");
    };
    const onKeyDown = function (event: { code: any }) {
        switch (event.code) {
            case "ArrowUp":
            case "KeyW":
                moveForward = true;
                handleDown();
                break;

            case "ArrowLeft":
            case "KeyA":
                moveLeft = true;
                handleDown();
                break;

            case "ArrowDown":
            case "KeyS":
                moveBackward = true;
                handleDown();
                break;

            case "ArrowRight":
            case "KeyD":
                moveRight = true;
                handleDown();
                break;
            case "ShiftLeft":
            case "ShiftRIGHT":
                pressShift = true;
                handleDown();
                break;
            case "Space":
                //可以跳的时候跳 y轴增加 开始跳跃时将可以跳跃状态设置为false
                if (canJump) velocity.y += JumpHeight;
                canJump = ejectorParams.isCanJump();
                break;
        }
    };

    const onKeyUp = function (event: { code: any }) {
        switch (event.code) {
            case "ArrowUp":
            case "KeyW":
                moveForward = false;
                break;

            case "ArrowLeft":
            case "KeyA":
                moveLeft = false;
                break;

            case "ArrowDown":
            case "KeyS":
                moveBackward = false;
                break;

            case "ArrowRight":
            case "KeyD":
                moveRight = false;
                break;
            case "ShiftLeft":
            case "ShiftRIGHT":
                pressShift = false;
                break;
        }
        //都没有按下才设置false
        if (!moveForward && !moveBackward && !moveLeft && !moveRight) {
            isMoving = false;
            acterPlayer.stop();
        }
    };

    document.addEventListener("keydown", onKeyDown);
    document.addEventListener("keyup", onKeyUp);

    const removeEventListener = () => {
        document.removeEventListener("keydown", onKeyDown);
        document.removeEventListener("keyup", onKeyUp);
    };

    return { removeEventListener };
}

/**
 * 初始化星球 - 将各星球的物体放到场景(scene)中
 */
const initStars = () => {
    for (const planet of [...planets, universe]) {
        planet.init();
        objects.push(...planet.withVolume);
        planet.withVolume.monitoringChanges = (...items: typeof FOREST.withVolume[number][]) => {
            objects.push(...items);
            window.render();
        };
        scene.add(planet.objects);
    }
    //@ts-ignore
    window.objects = objects;
};

/**
 * 初始化three场景 不能重复触发 会加载重复模型导致性能拉胯
 */
export const init = (parameters?: THREE.WebGLRendererParameters) => {
    if (init.alreadyInit) return;
    init.alreadyInit = true;

    intGlobalVariable();
    initScene();
    initWebWorker();
    initStars();
    init.mouseControls = initControls();
    stats.init();
    initWebGLRenderer(parameters);
    window.render();
};

init.mouseControls = {} as ReturnType<typeof initControls>;
init.alreadyInit = false;

/**
 * 确定进入星球再渲染(监听) 在之前只加载数据
 */
init.enter = () => {
    const keyPressListen = initKeyPressListen();

    init.mouseControls.lockMouse();

    const removeListener = () => {
        keyPressListen.removeEventListener();
    };
    window.render && window.render();
    return { removeListener, mouseControls: init.mouseControls };
};

/**
 * 处理
 * 进入哪个星球加载、渲染该星球的物体
 * 离开星球清除该星球内的物体
 */
const acterInWherePlanet = ActerInWherePlanet((curr, prev) => {
    console.log("进入", curr);
    console.log("离开", prev);
    // currentPlanet = planets.find((planet) => planet.name === curr)!;
    for (const planet of planets) {
        let visible = false;
        if (planet.name === curr) {
            currentPlanet = planet;
            // currentPlanet.polarAngle && controls?.changePolarAngle(currentPlanet.polarAngle);
            visible = true;
        }

        planet.objects.children.forEach((mesh) => {
            mesh.visible = visible;
        });
        //不论星球是否隐藏外壳都要显示
        planet.planet.visible = true;
    }
});

/**
 * 前往另一个星球
 */
export const toOtherPlanet = (name: string, renderBack: () => void) => {
    const target = planets.find((planet) => planet.name === name);
    if (!target) throw new Error("未获取到前往星球数据 " + name);
    console.time();
    target.objects.children.forEach((mesh) => {
        mesh.visible = true;
    });
    console.timeEnd();
    renderBack();
};

const acterPosition = new THREE.Vector3();

/**
 * 锁定鼠标时逐帧执行的操作 -- 核心逻辑
 */
function animate() {
    animationFrame = requestAnimationFrame(animate);
    //计算是否需要计算陷入地下 比如 站在高处就不必计算是否会陷入地下
    const isNeedCalculateUnderground = currentPlanet.underground.needCalculate(acterWrap.position.y);
    stats.update();
    const time = performance.now(); //每一帧的间隔时间 保证相同时间移动相同距离
    const delta = (time - prevTime) / 1000;
    acterPosition.copy(acterWrap.position);
    /**
     *  web worker中进行计算
     */
    myWorker.postMessage({
        work_type: "calculate",
        isNeedCalculateUnderground,
        currentPlanetName: currentPlanet.name,
        acterPosition,
    });

    nearCheck.calculateNearbyObjects(currentPlanet.mustBeDetected, [
        ...currentPlanet.withVolume,
        ...universe.withVolume,
    ]);
    const { nearbyObjects } = nearCheck;
    acterInWherePlanet.update(acterPosition);
    //根据boolean值 巧妙判断方向 决定下面移动的增量
    direction.z = Number(moveForward) - Number(moveBackward);
    direction.x = Number(moveRight) - Number(moveLeft);
    direction.normalize();

    //缓冲 慢慢把速度降下来 即停止移动还会向前滑一小段 数值慢慢都会归零
    velocity.x -= velocity.x * 10.0 * delta * acterScaleMultiple;
    velocity.z -= velocity.z * 10.0 * delta * acterScaleMultiple;
    velocity.y -= 5.8 * 100.0 * delta * acterScaleMultiple * acterScaleMultiple;
    //四个方位是否产生碰撞
    let leftCollide = 0;
    let rightCollide = 0;
    let forwardCollide = 0;
    let backCollide = 0;
    //碰撞检测
    if (moveForward) forwardCollide = directionCheck(controls, nearbyObjects, acterPosition, "forward").length;
    if (moveBackward) backCollide = directionCheck(controls, nearbyObjects, acterPosition, "back").length;
    if (moveLeft) leftCollide = directionCheck(controls, nearbyObjects, acterPosition, "left").length;
    if (moveRight) rightCollide = directionCheck(controls, nearbyObjects, acterPosition, "right").length;

    //如果不为0就表示按下了前或者后键
    if (direction.z) velocity.z -= direction.z * 400.0 * delta;
    if (direction.x) velocity.x -= direction.x * 400.0 * delta;
    //是否shift加速
    const quicken = pressShift ? 7 : 1;
    //未限制的移动距离
    rightDistance = -velocity.x * delta * quicken;
    forwardDistance = -velocity.z * delta * quicken;

    let uphillHeight = 0;
    if (isMoving) {
        const dir = dirType(moveForward, moveBackward, moveRight);
        const hdis = !!direction.z ? forwardDistance : rightDistance;
        [uphillHeight, hillAngle, isUphill] = uphillCheck(controls, dir, nearbyObjects, hdis);

        if (!enterKey && controls.moving) {
            enterKey = true;
            scaleToTarget(controls, acterWrap, cameraWrap);
        }
    } else {
        enterKey && (enterKey = false);
        controls.syncRotate && (controls.syncRotate = false);
    }

    !isFallIntoFloor && upAndFallCheck();

    if ((moveRight && rightCollide) || (moveLeft && leftCollide)) {
        rightDistance = isUphill ? rightDistance * hillAngle : 0;
        // console.log("--------collide--------", hillAngle, rightDistance);
    }
    if ((moveForward && forwardCollide) || (moveBackward && backCollide)) {
        forwardDistance = isUphill ? forwardDistance * hillAngle : 0;
        // console.log("--------collide--------", hillAngle, forwardDistance);
    }

    if (direction.x) controls.moveRight(rightDistance);
    if (direction.z) controls.moveForward(forwardDistance);

    let upDistance = velocity.y * delta + uphillHeight;
    isFallIntoFloor && (upDistance = currentPlanet.fallIntoFloor(upDistance, velocity));

    acterWrap.position.y += upDistance;
    cameraWrap.position.y += upDistance;

    //限制最低高度
    if (acterWrap.position.y < 0) {
        velocity.y = 0;
        acterWrap.position.y = 0;
        cameraWrap.position.y = 0;
        canJump = true;
    }

    /**
     * 是否有需要进入物体触发的条件
     */
    nearbyObjects.forEach((mesh) => {
        if (mesh.userData.nearbyCheck) {
            mesh.userData.nearbyCheck(acterPosition);
        }
    });

    animationControl.update();
    currentPlanet.ticks();
    prevTime = time;
    window.render();
}

/**
 * 检测头上与脚下
 */
function upAndFallCheck() {
    //不获取新的检测会出错待排查
    const acterPosition = acterWrap.position;
    const upCollideLength = upCollide(acterPosition, acterHeight, nearCheck.nearbyObjects).length;

    //如果头上有障碍并且在向上运动时 和下方检测一样 第一次反弹后第二帧还能检测到头上有物体 就导致一直在乘-1一直闪烁
    if (upCollideLength && velocity.y > 0) {
        velocity.y *= -1;
        console.log("碰头了");
        return;
    }

    //* 刚起跳时向下检测射线能检测到当前脚下的物体 所以会将canJump又变回true 实现二段跳
    //向上跳起时不用检测脚下
    if (velocity.y > 0) return;

    //脚下
    const fallMesh = fallCollide(acterPosition, nearCheck.nearbyObjects);
    const onObject = fallMesh[0];
    //如果下方没有物体不处理
    if (!onObject) return;
    //新的连续跳跃计数
    ejectorParams.recount();
    //上坡时不被缓慢下落干扰
    // if (isUphill && isMoving) return (velocity.y = 0);
    if (isUphill) return (velocity.y = 0);
    //射线的远端触碰到了物体就返回物体，如果碰到就停止下落就会造成浮空 因为射线没走完 在头就停下身子的高度就是浮空的高度
    //1 是允许误差范围即脚离下方物体的高度
    const dis = onObject.distance;
    if (dis > 1) {
        velocity.y = Math.max(-dis * 10 * acterScaleMultiple, velocity.y);
    } else {
        velocity.y = Math.max(0, velocity.y);
    }

    canJump = true;

    /**
     * 处理踩在物体上发生的绑定在物体身上的事件
     */
    if (onObject.object.userData.allow) {
        onObject.object.userData.effect();
    }
}

/**
 * 外部程序控制人物跳跃
 */
export const acterJump = (JumpHeight: number) => {
    velocity.y += JumpHeight;
};
